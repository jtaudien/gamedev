using UnityEngine;
using System.Collections;

public class MainMenu : MonoBehaviour
{
	private string instructionText = "Instruction:\nPress Left and Right Arrows to move.\n" +
		"Press Space to fire Wappon.";
	private int buttonWidth = 200;
	private int buttonHight = 50;
	
	public Texture backgroundTexture;
	
	// Use this for initialization
	void Start ()
	{
	
	}
	
	// Update is called once per frame
	void OnGUI ()
	{
		//GUI.DrawTexture(new Rect(0,0, Screen.width, Screen.hight), backgroundTexture);
		GUI.Label(new Rect(10,10,300,200), instructionText);
		
		//if (Input.anyKeyDown)
			
		if (GUI.Button(new Rect(Screen.width / 2 - buttonWidth / 2, 
							Screen.height / 2 - buttonHight / 2, buttonWidth, buttonHight), "Start Game")
			)
		{
			player.Score = 0;
			player.Lives = 3;
			Application.LoadLevel(1);
		}
	}
}

