using UnityEngine;
using System.Collections;

public class ItemProduction : MonoBehaviour {
	
	public Object item;
	public player ship;
	private float seconds = 1.5f;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	public void CreateItem()
	{
		Vector3 start = new Vector3(0,0,0);
		Instantiate(item,start,Quaternion.identity);
	}
	
	protected bool HandleItemDrop()
	{
		
		float level = player.Level_player;
		float lives = player.Lives;
		float score = player.Score;
		float shieldenergy = player.ShieldEnergy;
		
		return false;
	}
	
	
}
