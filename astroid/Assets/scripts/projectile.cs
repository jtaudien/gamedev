using UnityEngine;
using System.Collections;

public class projectile : MonoBehaviour {
	
	public float speed;
	public GameObject ExplosivPrefab;
	private Transform mytransform; //good for iphone coding, less updates, start position known
	private Enemy enemy;
	private GameObject[] Enemys;
	public EnemyProduction enemyProd;
	private HandleSceen sceen;
	// Use this for initialization
	void Start () 
	{
		//enemy = (Enemy)OtherObject.gameObject.GetComponent("Enemy");
		mytransform = transform;
	}
	
	// Update is called once per frame
	void Update () 
	{
		float moveprojectile = speed * Time.deltaTime;
		mytransform.Translate(Vector3.up * moveprojectile);
		
		//laser vanish if exceed screen borders
		if (mytransform.position.y > 5.3)
		{
			Destroy(gameObject);
		}
	}
	
	void OnCollisionEnter(Collision collision)
	{
		//Debug.Log("enter");
  		ContactPoint contact = collision.contacts[0];
		//Debug.DrawRay(contact.point, contact.normal, Color.white);
  		//Debug.Log (contact.point);
	}
	
	void OnTriggerEnter(Collider OtherObject)
	{
		//Debug.Log ("HIT!" + OtherObject.gameObject.name);
		if (OtherObject.tag == "enemy")
		{ 
			//Destroy(OtherObject.gameObject);	
			
			GameObject go = GameObject.Find("Main Camera");
			sceen = (HandleSceen) go.GetComponent(typeof(HandleSceen) );
			//GameObject.Find("HandleSceen").GetComponent(typeof(HandleSceen)).IncreaseDifficult();
			if (sceen != null)
				sceen.IncreaseDifficult();
			Debug.Log("FOUND");
			
			//go = GameObject.Find("Enemy");
			enemy = (Enemy) OtherObject.GetComponent(typeof(Enemy));
			Object clone;
            clone = Instantiate(ExplosivPrefab, enemy.transform.position, enemy.transform.rotation);
			
			//Debug.Log ("HIT! %f" + enemy.transform.position.x);
			//enemy.SetMovement();
			
			enemy.Destroy();
			
			go = GameObject.Find("EnemyProduction");
			enemyProd = (EnemyProduction) go.GetComponent(typeof(EnemyProduction));
			enemyProd.CreateInstance(1);
			
			Destroy(gameObject);
			Destroy(clone,1.5f);
			//Debug.Log (OtherObject.GetType());
			player.Score += 100;
			
			//Debug.Log ("ENEMIES "+enemy.GetCount());
			if (enemy.GetCount() <= 1)
			{
				//enemyProd = GetComponent<EnemyProduction>();
				//enemyProd = (EnemyProduction)OtherObject.gameObject.GetComponent("EnemyProduction");
			
         		//Debug.Log("enemyProd ");
			}
		}
	}
	
	
}
