using UnityEngine;
using System.Collections;

public class EnemyProduction : MonoBehaviour {
	
	public Object enemy;
	private float seconds = 1.5f;
	// Use this for initialization
	void Start () 
	{
	
	}
	
	// Update is called once per frame
	void Update () 
	{
	
		
		
	}
	
	public void CreateInstance(int n)
	{
		float val = Random.value;
		val = Random.Range(0f,2f);
		//Debug.Log ("VAL " + val.ToString());
		StartCoroutine(Wait (val, n));
	}
	
	IEnumerator Wait(float sec, int n) 
	{
		Vector3 start = new Vector3(0,0,0);
		int i = 0;
		while ( i < n)
		{
			yield return new WaitForSeconds(sec);
			Instantiate(enemy,start,Quaternion.identity);
			
			int number;
			number = enemy.GetInstanceID();
			//Debug.Log(number.ToString());
			i++;
		}
 		
	}
}
