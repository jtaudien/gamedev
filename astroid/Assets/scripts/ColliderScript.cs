using UnityEngine;
using System.Collections;

public class ColliderScript : MonoBehaviour {
	
	public player ship;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	void OnTriggerEnter(Collider OtherObject)
	{ 
		

		if (OtherObject.tag == "enemy" && ship.state == player.State.Playing)
		{	
			player.Lives --;
			Enemy enemy = (Enemy)OtherObject.gameObject.GetComponent("Enemy");
			//collision.relativeVelocity = new Vector3(collision.relativeVelocity.x, collision.relativeVelocity.y,0);
			OtherObject.collider.transform.localRotation = new Quaternion(0,0,0,1);
			enemy.transform.localRotation = new Quaternion(0,0,0,1);
			//enemy.SetMovement();
			enemy.Destroy();
			GameObject go = GameObject.Find("EnemyProduction");
			EnemyProduction enemyProd = (EnemyProduction) go.GetComponent(typeof(EnemyProduction));
			enemyProd.CreateInstance(1);
			//Destroy(enemy);
			ship.StartCoroutine(ship.DestroyShip());
		}
	}
}
