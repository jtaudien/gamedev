using UnityEngine;
using System.Collections;


public class Enemy : MonoBehaviour
{
	public float min_speed=2, max_speed=6;
	private float speed;
	private float x,y,z;
	public float Level_enemy = 1;
	
	private float minRotationSpeed = 60f;
	private float maxRotationSpeed =120f;
	private float currentRotationSpeed;
	private Vector3 rotAxis;
	private float minScale = 35f, maxScale = 75f, currentScaleX, currentScaleY, currentScaleZ;
	
	
	// Use this for initialization
	void Start ()
	{
		SetMovement();
	}

	// Update is called once per frame
	void Update ()
	{
		
		if (transform.position.y <= -5.5f)
		{
//			SetMovement ();
//			Vector3 pos = transform.position;
//			pos = new Vector3 (pos.x, pos.y, 0);
//			Debug.Log (pos);
			Destroy(gameObject);
			GameObject go = GameObject.Find("EnemyProduction");
			EnemyProduction enemyProd = (EnemyProduction) go.GetComponent(typeof(EnemyProduction));
			enemyProd.CreateInstance(1);
			
		}
		
		float rotationSpeed = currentRotationSpeed * Time.deltaTime;
		
		transform.Rotate(rotAxis*rotationSpeed);
		
		float enemyMove = speed * Time.deltaTime;//also acceleration could be added here
		transform.Translate(Vector3.down * enemyMove,Space.World);
		Vector3 pos2 = transform.position;
		//Debug.Log (pos2);
		
		

	}
	
	public void SetMovement ()
	{
		currentRotationSpeed = Random.Range(minRotationSpeed,maxRotationSpeed);
		rotAxis = new Vector3 (0,Random.Range(-1,1),Random.Range(-1,1));
		
		currentScaleX = Random.Range(minScale,maxScale);
		currentScaleY = Random.Range(minScale,maxScale);
		currentScaleZ = Random.Range(minScale,maxScale);
		
		speed = Random.Range(min_speed,max_speed);
		x = Random.Range(-5.85f,5.85f);
		y =	5.5f;
		z = 0f;
		
		transform.position = new Vector3(x,y,z);
		transform.localScale = new Vector3(currentScaleX, currentScaleY, currentScaleZ);
	}
	
	public void Destroy()
	{
	 	Destroy(gameObject);
	}
	
	public int GetCount()
	{
		int counter = 0;
		GameObject[] enemylist = GameObject.FindGameObjectsWithTag("enemy");
		counter = enemylist.GetLength(0);
			
		return counter;
	}
	

	
	void OnCollisionEnter(Collision collision)
	{
		//Debug.Log("enter");
  		ContactPoint contact = collision.contacts[0];
		
		
		//Debug.Log(contact.normal);
		
		//Debug.DrawRay(contact.point, contact.normal, Color.white);
  		//Debug.Log (contact.point);
	}

}