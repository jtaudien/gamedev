using UnityEngine;
using System.Collections;

public class Lose : MonoBehaviour
{
	private int buttonWidth = 200;
	private int buttonHight = 50;
	
	public Texture backgroundTexture;
	// Use this for initialization
	void Start ()
	{
	
	}
	
	// Update is called once per frame
	void Update ()
	{
	
	}
	
	void OnGUI ()
	{
		GUI.Label(new Rect(Screen.width / 2 - buttonWidth /2, 
							Screen.height / 2 - buttonHight * 2 , buttonWidth, buttonHight), "Your Score: " + player.Score.ToString() + " Points");
		//GUI.DrawTexture(new Rect(0,0, Screen.width, Screen.hight), backgroundTexture);
		if (GUI.Button(new Rect(Screen.width / 2 - buttonWidth / 2, 
							Screen.height / 2 - buttonHight / 2, buttonWidth, buttonHight), "Game Over!")
			)
		{
			Application.LoadLevel(0);
		}
	}
}

