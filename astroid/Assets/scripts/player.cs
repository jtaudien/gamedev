using UnityEngine;
using System.Collections;

public class player : MonoBehaviour {
	
	//
	public enum State
	{
		Playing,
		Explosiv,
		invincible
	}
	
	public State state = State.Playing;
	//public Animation animation;
	// Use this for initialization
	public float x_0=0;
	public float y_0=-4;
	public static float player_speed=7;
	public GameObject projetile_pre;
	//public Rigidbody rocketPrefab;
	public GameObject explosiv_pre;
	public GameObject shields_pre;
	
	public static float Level_player = 1;
	public static int Score = 0;
	public static int Lives = 3;
	public static int ShieldEnergy = 100;
	
	public float ProjectileOffset = 1.2f;
	private float shipInvisableTime = 1f;
	private float shipMoveOnToScreenSpeed = 5f;
	private float blinkRate = 0.1f;
	private int timesToBlinked = 10;
	private int blinkCount = 0;
	private bool rolled = false;
	private float direction = 1;
	private Shields shield;
	protected Animator animator;
	
	void Start () 
	{
		transform.position = new Vector3(x_0,y_0,transform.position.z);
		Instantiate(shields_pre,transform.position,Quaternion.identity);	
		GameObject go = GameObject.FindWithTag("Shields");
		shield = (Shields)go.GetComponent("Shields");
		shield.renderer.material.mainTextureScale = new Vector2 (0f,0f);
		
		animator = GetComponent<Animator>();
		if (animator != null)
			Debug.Log ("DONE");
				//load level parameters (level class)
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (state != State.Explosiv)
		{
			float Player2Move = Input.GetAxis("Horizontal") * player_speed * Time.deltaTime;
			
			transform.Translate(Vector3.right * Player2Move);
			transform.position = new Vector3(transform.position.x,transform.position.y,0);
			
			
			
			shield.transform.position = new Vector3(transform.position.x,transform.position.y,0);
			
			Debug.Log (Vector3.right * Player2Move);
			if ((rolled == false) && (Vector3.right * Player2Move).x < 0)
			{
				transform.Rotate(0f, 0f, 35f);
				shield.transform.Rotate(0f, 35f, 0f);
				rolled = true;
				direction = -1;
			}
			else if ((rolled == false) && (Vector3.right * Player2Move).x > 0)
			{
				transform.Rotate(0f, 0f, -35f);
				shield.transform.Rotate(0f, -35f , 0f);
				rolled = true;
				direction = 1;
			}
			else if ((rolled == true) && (Vector3.right * Player2Move).x == 0)
			{
				transform.Rotate(0f,0f,35f*direction);
				shield.transform.Rotate(0f, 35f*direction, 0f);
				rolled = false;
				direction = 1;
			}
			
			if (transform.position.x <= -6 || transform.position.x >= 6)
			{
				transform.position = new Vector3(-transform.position.x,transform.position.y, transform.position.z);
				shield.transform.position = new Vector3(-transform.position.x,transform.position.y, transform.position.z);
			}
			
			if (Input.GetKeyDown("space"))
			{
				Vector3 pos = new Vector3(transform.position.x,  transform.position.y + collider.bounds.size.y / 2, transform.position.z);
				Instantiate(projetile_pre,pos,Quaternion.identity);	
			}
			if (Input.GetKeyDown("e"))
			{
					//animator.SetBool("bang", true );
			}
			else
			{
				//animator.SetBool("bang", false);
				
			}

		}
		
		//level implementation
		//the higher the level the higher the numer of enemys and the difficulty to distroy
		//point counter to endboss etc
		
		int number;
		number = gameObject.GetInstanceID();
		//Debug.Log("Player");
		//Debug.Log(number.ToString());
	}
	
//	void OnGUI()
//	{
//		GUI.Label(new Rect(10, 10, 200, 20), "Score: "+player.Score.ToString());
//		GUI.Label(new Rect(10, 30, 60, 20), "Lives: "+player.Lives.ToString());			
//	}
	
	
	
  // void OnCollisionEnter(Collision collision)
//	{
//		
//		//nicht zu gebrauchen in einer 2D ohne enemy distroy
//		if (collision.gameObject.tag == "enemy" && state == State.Playing)
//		{	
//			//Debug.Log("InPlayer");
//			//player.Lives --;
//			Enemy enemy = (Enemy)collision.gameObject.GetComponent("Enemy");
//			//collision.relativeVelocity = new Vector3(collision.relativeVelocity.x, collision.relativeVelocity.y,0);
//			collision.collider.transform.localRotation = new Quaternion(0,0,0,1);
//			enemy.transform.localRotation = new Quaternion(0,0,0,1);
//			//enemy.SetMovement();
//			enemy.Destroy();
//			GameObject go = GameObject.Find("EnemyProduction");
//			EnemyProduction enemyProd = (EnemyProduction) go.GetComponent(typeof(EnemyProduction));
//			enemyProd.CreateInstance(1);
//			//Destroy(enemy);
//			StartCoroutine(DestroyShip());
//			
//
//		}
//  		
//	}
	
	public IEnumerator DestroyShip()
	{
		state = State.Explosiv;
		shield.renderer.material.mainTextureScale = new Vector2 (1f,1f);
		
		Object clone;
        clone = Instantiate(explosiv_pre, transform.position, Quaternion.identity);
		Destroy(clone,1.0f);
		gameObject.renderer.enabled = false; //turn off HIDE
		shield.renderer.enabled = false;
		
		transform.position = new Vector3(0f, -5.9f, 1f);
		shield.transform.position = new Vector3(0f, -5.9f, 1f);
		
		yield return new WaitForSeconds(shipInvisableTime); //WAIT
		if (player.Lives > 0)
		{
			
			gameObject.renderer.enabled = true; //SHOW
			while (transform.position.y <= -4.5)
			{
				float amtToMove = shipMoveOnToScreenSpeed * Time.deltaTime;
				transform.position = new Vector3(0f, transform.position.y + amtToMove, transform.position.z);
				shield.transform.position = new Vector3(0f, transform.position.y + amtToMove, transform.position.z);
				yield return 0; //movement is not done in one update
			}
			
			state = State.invincible;
			
			while (blinkCount < timesToBlinked)
			{
				gameObject.renderer.enabled = !gameObject.renderer.enabled;
				shield.renderer.enabled = !shield.renderer.enabled;
				if (gameObject.renderer.enabled)
				{
					blinkCount ++;
					Debug.Log("I am HERE");
				}
				yield return new WaitForSeconds(blinkRate); //WAIT
			}
			blinkCount = 0;
			state = State.Playing;
			shield.renderer.material.mainTextureScale = new Vector2 (0f,0f);
		}
		else 
			Application.LoadLevel(2);

	}
	
}
